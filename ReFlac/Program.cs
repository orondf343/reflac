﻿using System.Diagnostics;
using System.Reflection;

var flacExe = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? "", "flac.exe");
var ffmpegExe = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? "", "ffmpeg.exe");

Console.WriteLine("ReFlac by OronDF343");
if (!File.Exists(flacExe))
{
    Console.WriteLine($"ERROR: Can't find flac.exe at {flacExe}");
}
if (!File.Exists(ffmpegExe))
{
    Console.WriteLine($"ERROR: Can't find ffmpeg.exe at {ffmpegExe}");
}

if (args.Length < 1)
{
    Console.WriteLine("ERROR: No directories specified!");
    return;
}

var argsOffset = 0;
Console.WriteLine();
var thOverride = int.TryParse(args[argsOffset], out var threads);
if (thOverride)
    ++argsOffset;
else
    threads = Environment.ProcessorCount;
Console.WriteLine($"Using {threads} threads");
Console.WriteLine();

var wait = false;
if (args[argsOffset] == "-w")
{
    wait = true;
    ++argsOffset;
}

if (args.Length < argsOffset + 1)
{
    Console.WriteLine("ERROR: No directories specified!");
    return;
}

var flacs = new List<string>();
for (var i = argsOffset; i < args.Length; i++)
{
    if (Directory.Exists(args[i]))
    {
        Console.WriteLine($"Adding files from directory: {args[i]}");
        flacs.AddRange(Directory.EnumerateFiles(args[i], "*.flac", SearchOption.AllDirectories));
    }
    else if (args[i].EndsWith(".flac", StringComparison.OrdinalIgnoreCase) && File.Exists(args[i]))
    {
        flacs.Add(args[i]);
    }
    else
    {
        Console.WriteLine($"WARNING: Directory {args[i]} does not exist!");
    }
}

var totalOldSize = 0L;
var totalNewSize = 0L;

await Parallel.ForEachAsync(flacs.Select((f, i) => (File: f, Id: i)), new ParallelOptions { MaxDegreeOfParallelism = threads }, async (f, _) =>
{
    var oldSize = new FileInfo(f.File).Length;
    var newFile = Path.Combine(Path.GetDirectoryName(f.File), Guid.NewGuid().ToString() + ".flac");
    var success = false;
    if (await StartFFmpegAsync(f.Id, f.File, newFile))
    {
        if (await StartFlacAsync(f.Id, newFile, f.File))
        {
            success = true;
        }
    }
    File.Delete(newFile);
    if (success)
    {
        var newSize = new FileInfo(f.File).Length;
        Interlocked.Add(ref totalOldSize, oldSize);
        Interlocked.Add(ref totalNewSize, newSize);
        var diff = oldSize - newSize;
        var percent = ((double) diff / oldSize) * 100;
        Console.WriteLine($"[{f.Id}] Saved {FormatFileSize(diff)} ({FormatPercent(percent)})");
    }
    else
    {
        Console.WriteLine($"[{f.Id}] ERROR: Failed to run FFmpeg: {f.File}");
    }
});

Console.WriteLine();
Console.WriteLine();
Console.WriteLine("Complete!");

var totalDiff = totalOldSize - totalNewSize;
var totalPercent = totalOldSize == 0 ? 0 : ((double)totalDiff / totalOldSize) * 100;
Console.WriteLine($"Saved {FormatFileSize(totalDiff)} ({FormatPercent(totalPercent)})");

if (wait)
{
    Console.WriteLine();
    Console.Write("Press any key to exit...");
    Console.ReadKey(true);
}

async Task<bool> StartFFmpegAsync(int id, string f, string o)
{
    Console.WriteLine($"[{id}] Removing padding: {f}");
    var p = new Process
    {
        StartInfo = new ProcessStartInfo(ffmpegExe, $"-i \"{f}\" -c:a copy -c:v copy -y -v warning -stats \"{o}\"")
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            WindowStyle = ProcessWindowStyle.Hidden,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        }
    };

    p.OutputDataReceived += (_, e) => Console.WriteLine($"[{id}:STDOUT] {e.Data}");
    p.ErrorDataReceived += (_, e) => Console.WriteLine($"[{id}:STDERR] {e.Data}");

    p.Start();
    p.BeginOutputReadLine();
    p.BeginErrorReadLine();

    await p.WaitForExitAsync();
    Console.WriteLine($"[{id}] ffmpeg has exited with code {p.ExitCode}");
    return p.ExitCode == 0;
}

async Task<bool> StartFlacAsync(int id, string f, string o = null)
{
    o ??= f;
    Console.WriteLine($"[{id}] Encoding: {o}");
    var p = new Process
    {
        StartInfo = new ProcessStartInfo(flacExe, $"-8 -e -f \"{f}\" -o \"{o}\"")
        {
            CreateNoWindow = true,
            UseShellExecute = false,
            WindowStyle = ProcessWindowStyle.Hidden,
            RedirectStandardOutput = true,
            RedirectStandardError = true
        }
    };

    p.OutputDataReceived += (_, e) => Console.WriteLine($"[{id}:STDOUT] {e.Data}");
    p.ErrorDataReceived += (_, e) => Console.WriteLine($"[{id}:STDERR] {e.Data}");

    p.Start();
    p.BeginOutputReadLine();
    p.BeginErrorReadLine();

    await p.WaitForExitAsync();
    Console.WriteLine($"[{id}] flac has exited with code {p.ExitCode}");
    return p.ExitCode == 0;
}

string FormatPercent(double percent)
{
    if (Math.Abs(percent) < 10d)
        return $"{percent:F2}%";
    else if (Math.Abs(percent) < 100d)
        return $"{percent:F1}%";
    else
        return $"{percent:F0}%";
}

string FormatFileSize(long bytes)
{
    if (Math.Abs(bytes) < 1000L)
        return $"{bytes} bytes";
    else if (Math.Abs(bytes) < 10L * 1024L)
        return $"{bytes / 1024d:F2} KiB";
    else if (Math.Abs(bytes) < 100L * 1024L)
        return $"{bytes / 1024d:F1} KiB";
    else if (Math.Abs(bytes) < 1000L * 1024L)
        return $"{bytes / 1024d:F0} KiB";
    else if (Math.Abs(bytes) < 10L * 1024L * 1024L)
        return $"{bytes / 1024d / 1024d:F2} MiB";
    else if (Math.Abs(bytes) < 100L * 1024L * 1024L)
        return $"{bytes / 1024d / 1024d:F1} MiB";
    else if (Math.Abs(bytes) < 1000L * 1024L * 1024L)
        return $"{bytes / 1024d / 1024d:F0} MiB";
    else if (Math.Abs(bytes) < 10L * 1024L * 1024L * 1024L)
        return $"{bytes / 1024d / 1024d / 1024d:F2} GiB";
    else if (Math.Abs(bytes) < 100L * 1024L * 1024L * 1024L)
        return $"{bytes / 1024d / 1024d / 1024d:F1} GiB";
    else
        return $"{bytes / 1024d / 1024d / 1024d:F0} GiB";
}
