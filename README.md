# ReFlac #

Simple command-line tool to re-encode FLAC files in place using the highest possible compression options (while preserving all existing tags).
A number of processes will be run concurrently (by default equal to the number of CPU cores), and ALL subdirectories will be processed!

Usage: ReFlac \[num_processes] folder1 \[folder2 ...]
